PARAM([Parameter(Mandatory=$true, Position=1)][array] $hashObjectName, [string] $hashType = "md5", [switch] $recurse, [switch] $noisey, [switch] $wp, [array] $type="", [array] $exc="", [switch] $bp)

#getfciv.ps1 c:\someDir, c:\anotherDir, e:\aFile.ext -recurse -exclude .exe
####################################################################################################
# FUNCTIONS
####################################################################################################
function computeHash([string] $fileName){
  #Use -LiteralPath to cover instances where the file name contains powershell special characters that are valid Windows file name characters
  $hashObject = get-item -LiteralPath $fileName
  if(($hashObject -ne $null) -and !($hashObject.psiscontainer)){
    try{
        $fileStream = new-object System.IO.FileStream($hashObject, [System.IO.FileMode]::Open, [System.IO.FileAccess]::Read)
    }catch [Exception]{
      #Return a bogus hash for the record.
      return "z" * 20;
    }
  }

  #There was a problem opening a FileStream object so return a fake hash
  if($fileStream -eq $null){
    return "z" * 32
  }

  $md5Object = new-object System.Security.Cryptography.MD5CryptoServiceProvider

  $md5ByteArray = $md5Object.ComputeHash($fileStream)

  $fileStream.close();

  $strBuilder = new-object System.Text.StringBuilder

  foreach($b in $md5ByteArray){
    $strBuilder.Append($b.ToString("x2")) | out-null
  }

  return $strBuilder.ToString()
}

#---------------------------------------------------------------------------------------------------
function ProcessFile($fileObj){
  if($wp){
    $key = $fileObj.name
  }else{
    $key = $fileObj.fullName
  }

  if($bp){
    $key = $key.Replace($hashObjectBaseName, "")
  }

  $hashValue = computeHash($fileObj.fullName)

  $hashValues.add($key,$hashValue)
  if($noisey){
     Write-Host "$hashValue $key"
  }
}
####################################################################################################
# END FUNCTIONS
####################################################################################################

if($noisey){
   Write-Host "Start time: $(get-date -format G)`n"
}

$hashValues = $null
$hashValues = @{}
$hashArray = @()

foreach($hObj in $hashObjectName){
  if(! (test-path $hObj)){
    continue;
  }

  #No need to use -LiteralPath since PowerShell will convert it to a FileInfo object then manually escape the string when casted
  $hashObject = get-item $hObj

  if($hashObject.psiscontainer){
    $hashObjectBaseName = $hashObject.FullName
    $hashArray = ls $hashObject -recurse:$recurse -include $type -exclude $exc |? {-not $_.psiscontainer}
  }else{
    $hashObjectBaseName = $hashObject.DirectoryName
    $hashArray = $hashObject
  }

  if(($hashArray -eq $null) -or ($hashArray.count -eq 0)){
    continue;
  }

  if(! $hashObjectBaseName.endsWith("\")){
    $hashObjectBaseName += "\"
  }

  if(!$bp){
    $hashObjectBaseName = ""
  }

  if($noisey -and $bp){
     Write-Host "Trimming base path of $hashObjectBaseName"
  }

  foreach($f in $hashArray){
    ProcessFile $f
  }

  if($noisey){
    Write-Host ""
  }
}

if(($hashArray -eq $null) -or ($hashArray.count -eq 0)){
  Write-Host "Please specify a valid file or folder path to hash" -backgroundColor "black" -foregroundColor "yellow"
}

if($noisey){
  Write-Host "End Time..: $(get-date -format G)`n"
}

$hashValues